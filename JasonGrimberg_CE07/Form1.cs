﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

/// <summary>
/// Jason Grimberg
/// CE07 JSON and Web Connectivity
/// An application that can download JSON data from a Web source.
/// Main Form
/// </summary>
/// 
namespace JasonGrimberg_CE07
{
    public partial class MainForm : Form
    {
        // Add some instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();

        // API starting point
        string apiStartingPoint = "http://mdv-vfw.com/";

        // API ending point
        string apiEndPoint;

        // Main form initialization
        public MainForm()
        {
            InitializeComponent();
        }

        // When checked the status will change
        private void rbClassASD_CheckedChanged(object sender, EventArgs e)
        {
            lblClassAstric.Visible = false;
            tsStatusLabel.Text = "";
        }

        // When checked the status will change
        private void rbClassVFW_CheckedChanged(object sender, EventArgs e)
        {
            lblClassAstric.Visible = false;
            tsStatusLabel.Text = "";
        }

        // Exit button to exit the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Button to download and display the data
        private void btnDownloadAndDisplay_Click(object sender, EventArgs e)
        {
            // Go to method that checks the Internet connection
            bool connectionStatus = CheckConnection();

            // Error handling to make sure a class is selected
            if(rbClassASD.Checked == true || rbClassVFW.Checked == true)
            {
                // Run the build api and read the data
                if (connectionStatus == true)
                {
                    BuildClassAPI();

                    ReadClassAPIData();
                }
                // Throw an error to tell the user what is happening
                else if (connectionStatus == false)
                {
                    MessageBox.Show("No Internet connection", "Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            // Tell the user what they did wrong and show where they can fix it
            else
            {
                // Show Astrix where they can fix their mistake
                lblClassAstric.Visible = true;
                // Tell the user what is going on
                MessageBox.Show("Please choose your class", "Oops", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        // New menu item to clear the form
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call method to clear the form
            ClearAll();
        }

        // Save menu item to save out to text file
        private void menuSaveMain_Click(object sender, EventArgs e)
        {
            // Create an if statement if the user did not fill out all of the fields
            if(!(tbClassName.Text == "" && tbCourseCode.Text == "" && tbCourseDesc.Text == "" 
                && numMonth.Value == 0 && numCreditHours.Value == 0))
            {
                // Use the SaveFileDialog to open a save dialog box
                using (var sfd = new SaveFileDialog())
                {
                    // Show only text files to write out to
                    sfd.Filter = "Text files (*.txt)|*.txt";

                    // Change the title of the save dialog
                    sfd.Title = "Save JSON to text";

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        // Write out to text file
                        File.WriteAllText(sfd.FileName, "%\r\n" + tbClassName.Text + "\r\n" + tbCourseCode.Text + "\r\n"
                            + tbCourseDesc.Text + "\r\n" + numMonth.Value + "\r\n" + numCreditHours.Value);

                        // Pop-up showing that the save was successful
                        MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK);
                    }
                }
            }
            // Show message box if all fields are not filled out
            else
            {
                MessageBox.Show("Please fill out all information before you save.", "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Load menu item to load from text file
        private void menuLoad_Click(object sender, EventArgs e)
        {
            // Call load method
            LoadNewFile();
        }

        // ---------------------------------------------------------------------
        // Methods
        // ---------------------------------------------------------------------
        // Method to check if you are connected to the Internet
        private bool CheckConnection()
        {
            // Try to connect to the web
            try
            {
                using (apiConnection = new WebClient())
                {
                    // Try to go to Google for a connection
                    using (apiConnection.OpenRead("http://www.google.com/"))
                    {
                        // If true then this will return true
                        return true;
                    }
                }
            }
            // Catch if this application cannot ping Google
            catch
            {
                return false;
            }
        }

        // Method to read the data
        private void ReadClassAPIData()
        {
            // Download all data as string
            var apiClassData = apiConnection.DownloadString(apiEndPoint);

            // Convert the string to an object
            JObject o = JObject.Parse(apiClassData);

            // Grab the specific data in the object for class title
            string classTitle = o["class"]["course_name_clean"].ToString();

            // Add the title to the form
            tbClassName.Text = classTitle;

            // Grab the specific data in the object for class code
            string classCode = o["class"]["program_code"].ToString();

            // Add the course code to the form
            tbCourseCode.Text = classCode;

            // Grab the specific data in the object for course description
            string classDescription = o["class"]["course_description"].ToString();

            // Add vertical scroll bars to the text-box
            tbCourseDesc.ScrollBars = ScrollBars.Vertical;

            // Show the text of the description
            tbCourseDesc.Text = classDescription;

            // Grab the specific data in the object for month
            // Parse out the correct data for the number
            int classMonth = Int32.Parse(o["class"]["sequence"].ToString());

            // Add it to the form
            numMonth.Value = classMonth;

            // Grab the specific data in the object for credit hours
            // Parse out the current data for the number
            int classCreditHours = Int32.Parse(o["class"]["credit"].ToString());

            // Add that data to the form
            numCreditHours.Value = classCreditHours;

        }

        // Method to clear data fields
        private void ClearAll()
        {
            // Clear all of the data fields on the main form
            tbClassName.Clear();
            tbCourseCode.Clear();
            tbCourseDesc.Clear();
            rbClassASD.Checked = false;
            rbClassVFW.Checked = false;
            numCreditHours.Value = 0;
            numMonth.Value = 0;
            tsStatusLabel.Text = "";
        }
        
        // Method to call which radio button is selected
        private string ReturnClassString()
        {
            string classString = "";
            // Check to see which radio button is checked
            // to return the correct string for the json file
            if (rbClassASD.Checked == true)
            {
                classString = "asd";
            }
            else if (rbClassVFW.Checked == true)
            {
                classString = "vfw";
            }

            return classString;
        }

        // Method to build the api string
        private void BuildClassAPI()
        {
            // Grab the class string
            string classString = ReturnClassString();

            // Complete the class API string
            apiEndPoint = apiStartingPoint + classString + ".json";
            
        }

        // Method to load new file
        private void LoadNewFile()
        {
            // Open dialog new instance
            OpenFileDialog saveDialog = new OpenFileDialog();

            // Change the title of the open dialog
            saveDialog.Title = "Open User Text File";

            // Filter out just text files to open
            saveDialog.Filter = "TXT files|*.txt";

            // Show the initial directory when the open dialog opens up
            saveDialog.InitialDirectory = @"C:\";

            // If statement for the opening of the open dialog
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveDialog.FileName;

                // Create an array of all the lines in the text box
                string[] filelines = File.ReadAllLines(fileName);

                // If statement to check first line of code for custom JSON
                if(filelines[0] != "%")
                {
                    MessageBox.Show("Not correct file format.");
                    return;
                }

                // List array of the course save file
                List<Courses> courseList = new List<Courses>();
                int linesPerUser = 5;
                int currUserLine = 0;

                // Parse line by line into instance of users class
                Courses courses = new Courses();
                for (int a = 0; a < filelines.Length; a++)
                {
                    // Check each line of the file that you are loading
                    if (a != 0 && a % linesPerUser == 0)
                    {
                        courseList.Add(courses);
                        courses = new Courses();
                        currUserLine = 1;
                    }
                    // Move to the next line of the file
                    else
                    {
                        currUserLine++;
                    }

                    // Switch statement to grab each of the lines of code
                    // from text file and populating the form
                    switch (currUserLine)
                    { 
                        case 1:
                            courses.CourseName = filelines[1].Trim();
                            tbClassName.Text = courses.CourseName;
                            if (courses.CourseName == "Advanced Scalable Data Infrastructures")
                            {
                                rbClassASD.Checked = true;
                            }
                            else if(courses.CourseName == "Visual Frameworks")
                            {
                                rbClassVFW.Checked = true;
                            }
                            break;
                        case 2:
                            courses.CourseCode = filelines[2].Trim();
                            tbCourseCode.Text = courses.CourseCode;
                            break;
                        case 3:
                            courses.CourseDescription = filelines[3].Trim();
                            tbCourseDesc.Text = courses.CourseDescription;
                            tbCourseDesc.ScrollBars = ScrollBars.Vertical;
                            break;
                        case 4:
                            courses.CourseMonth = Decimal.Parse(filelines[4].Trim());
                            numMonth.Value = courses.CourseMonth;
                            break;
                        case 5:
                            courses.CourseCredits = Decimal.Parse(filelines[5].Trim());
                            numCreditHours.Value = courses.CourseCredits;
                            MessageBox.Show("Load Complete.");
                            break;
                    }
                }
            }
        }
    }
}
