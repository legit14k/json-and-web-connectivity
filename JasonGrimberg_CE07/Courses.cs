﻿/// <summary>
/// Jason Grimberg
/// Courses class to hold all data
/// and to get and set the data
/// </summary>
/// 
namespace JasonGrimberg_CE07
{
    class Courses
    {
        // Variables for new course
        private string courseName;
        private string courseCode;
        private string courseDescription;
        private decimal courseMonth;
        private decimal courseCredits;

        public string CourseName { get => courseName; set => courseName = value; }
        public string CourseCode { get => courseCode; set => courseCode = value; }
        public string CourseDescription { get => courseDescription; set => courseDescription = value; }
        public decimal CourseMonth { get => courseMonth; set => courseMonth = value; }
        public decimal CourseCredits { get => courseCredits; set => courseCredits = value; }

        public void courseConst() // course constructors
        {
            CourseName = "";
            CourseCode = "";
            CourseDescription = "";
            CourseMonth = 0.00m;
            CourseCredits = 0.00m;
        }
            

    }
}
