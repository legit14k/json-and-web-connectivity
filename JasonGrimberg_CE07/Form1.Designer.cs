﻿namespace JasonGrimberg_CE07
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuMainWindow = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveMain = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbData = new System.Windows.Forms.GroupBox();
            this.btnDownloadAndDisplay = new System.Windows.Forms.Button();
            this.rbClassASD = new System.Windows.Forms.RadioButton();
            this.rbClassVFW = new System.Windows.Forms.RadioButton();
            this.tbClassName = new System.Windows.Forms.TextBox();
            this.tbCourseCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numCreditHours = new System.Windows.Forms.NumericUpDown();
            this.tbCourseDesc = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numMonth = new System.Windows.Forms.NumericUpDown();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblClassAstric = new System.Windows.Forms.Label();
            this.menuMainWindow.SuspendLayout();
            this.gbData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCreditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMainWindow
            // 
            this.menuMainWindow.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuMainWindow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuMainWindow.Location = new System.Drawing.Point(0, 0);
            this.menuMainWindow.Name = "menuMainWindow";
            this.menuMainWindow.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuMainWindow.Size = new System.Drawing.Size(599, 33);
            this.menuMainWindow.TabIndex = 0;
            this.menuMainWindow.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLoad,
            this.menuSaveMain,
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // menuLoad
            // 
            this.menuLoad.Name = "menuLoad";
            this.menuLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.menuLoad.Size = new System.Drawing.Size(210, 30);
            this.menuLoad.Text = "&Load...";
            this.menuLoad.Click += new System.EventHandler(this.menuLoad_Click);
            // 
            // menuSaveMain
            // 
            this.menuSaveMain.Name = "menuSaveMain";
            this.menuSaveMain.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuSaveMain.Size = new System.Drawing.Size(210, 30);
            this.menuSaveMain.Text = "&Save...";
            this.menuSaveMain.Click += new System.EventHandler(this.menuSaveMain_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.lblClassAstric);
            this.gbData.Controls.Add(this.textBox1);
            this.gbData.Controls.Add(this.tbCourseDesc);
            this.gbData.Controls.Add(this.numMonth);
            this.gbData.Controls.Add(this.numCreditHours);
            this.gbData.Controls.Add(this.label5);
            this.gbData.Controls.Add(this.label4);
            this.gbData.Controls.Add(this.label3);
            this.gbData.Controls.Add(this.label7);
            this.gbData.Controls.Add(this.label2);
            this.gbData.Controls.Add(this.label1);
            this.gbData.Controls.Add(this.tbCourseCode);
            this.gbData.Controls.Add(this.tbClassName);
            this.gbData.Controls.Add(this.rbClassVFW);
            this.gbData.Controls.Add(this.rbClassASD);
            this.gbData.Controls.Add(this.btnDownloadAndDisplay);
            this.gbData.ForeColor = System.Drawing.Color.White;
            this.gbData.Location = new System.Drawing.Point(12, 59);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(577, 432);
            this.gbData.TabIndex = 1;
            this.gbData.TabStop = false;
            this.gbData.Text = "Course Selection";
            // 
            // btnDownloadAndDisplay
            // 
            this.btnDownloadAndDisplay.ForeColor = System.Drawing.Color.Black;
            this.btnDownloadAndDisplay.Location = new System.Drawing.Point(184, 381);
            this.btnDownloadAndDisplay.Name = "btnDownloadAndDisplay";
            this.btnDownloadAndDisplay.Size = new System.Drawing.Size(195, 39);
            this.btnDownloadAndDisplay.TabIndex = 8;
            this.btnDownloadAndDisplay.Text = "Download And Display";
            this.btnDownloadAndDisplay.UseVisualStyleBackColor = true;
            this.btnDownloadAndDisplay.Click += new System.EventHandler(this.btnDownloadAndDisplay_Click);
            // 
            // rbClassASD
            // 
            this.rbClassASD.AutoSize = true;
            this.rbClassASD.ForeColor = System.Drawing.Color.White;
            this.rbClassASD.Location = new System.Drawing.Point(203, 25);
            this.rbClassASD.Name = "rbClassASD";
            this.rbClassASD.Size = new System.Drawing.Size(68, 24);
            this.rbClassASD.TabIndex = 1;
            this.rbClassASD.TabStop = true;
            this.rbClassASD.Text = "ASD";
            this.rbClassASD.UseVisualStyleBackColor = true;
            this.rbClassASD.CheckedChanged += new System.EventHandler(this.rbClassASD_CheckedChanged);
            // 
            // rbClassVFW
            // 
            this.rbClassVFW.AutoSize = true;
            this.rbClassVFW.ForeColor = System.Drawing.Color.White;
            this.rbClassVFW.Location = new System.Drawing.Point(203, 55);
            this.rbClassVFW.Name = "rbClassVFW";
            this.rbClassVFW.Size = new System.Drawing.Size(70, 24);
            this.rbClassVFW.TabIndex = 2;
            this.rbClassVFW.TabStop = true;
            this.rbClassVFW.Text = "VFW";
            this.rbClassVFW.UseVisualStyleBackColor = true;
            this.rbClassVFW.CheckedChanged += new System.EventHandler(this.rbClassVFW_CheckedChanged);
            // 
            // tbClassName
            // 
            this.tbClassName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbClassName.Location = new System.Drawing.Point(173, 85);
            this.tbClassName.Name = "tbClassName";
            this.tbClassName.Size = new System.Drawing.Size(390, 26);
            this.tbClassName.TabIndex = 3;
            // 
            // tbCourseCode
            // 
            this.tbCourseCode.Location = new System.Drawing.Point(173, 118);
            this.tbCourseCode.Name = "tbCourseCode";
            this.tbCourseCode.Size = new System.Drawing.Size(175, 26);
            this.tbCourseCode.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Class Selection: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(48, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Course Code: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Course Description: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(96, 310);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Month: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(52, 342);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Credit Hours: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(56, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Class Name: ";
            // 
            // numCreditHours
            // 
            this.numCreditHours.Location = new System.Drawing.Point(173, 342);
            this.numCreditHours.Name = "numCreditHours";
            this.numCreditHours.Size = new System.Drawing.Size(67, 26);
            this.numCreditHours.TabIndex = 7;
            // 
            // tbCourseDesc
            // 
            this.tbCourseDesc.Location = new System.Drawing.Point(173, 153);
            this.tbCourseDesc.Multiline = true;
            this.tbCourseDesc.Name = "tbCourseDesc";
            this.tbCourseDesc.Size = new System.Drawing.Size(390, 148);
            this.tbCourseDesc.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(290, 342);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(281, 26);
            this.textBox1.TabIndex = 9;
            this.textBox1.Visible = false;
            // 
            // numMonth
            // 
            this.numMonth.Location = new System.Drawing.Point(173, 308);
            this.numMonth.Name = "numMonth";
            this.numMonth.Size = new System.Drawing.Size(67, 26);
            this.numMonth.TabIndex = 7;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 494);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(599, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsStatusLabel
            // 
            this.tsStatusLabel.Name = "tsStatusLabel";
            this.tsStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblClassAstric
            // 
            this.lblClassAstric.AutoSize = true;
            this.lblClassAstric.Enabled = false;
            this.lblClassAstric.ForeColor = System.Drawing.Color.DarkRed;
            this.lblClassAstric.Location = new System.Drawing.Point(20, 29);
            this.lblClassAstric.Name = "lblClassAstric";
            this.lblClassAstric.Size = new System.Drawing.Size(15, 20);
            this.lblClassAstric.TabIndex = 10;
            this.lblClassAstric.Text = "*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(599, 516);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gbData);
            this.Controls.Add(this.menuMainWindow);
            this.MainMenuStrip = this.menuMainWindow;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Course";
            this.menuMainWindow.ResumeLayout(false);
            this.menuMainWindow.PerformLayout();
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCreditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMainWindow;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuLoad;
        private System.Windows.Forms.ToolStripMenuItem menuSaveMain;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.TextBox tbCourseCode;
        private System.Windows.Forms.TextBox tbClassName;
        private System.Windows.Forms.RadioButton rbClassVFW;
        private System.Windows.Forms.RadioButton rbClassASD;
        private System.Windows.Forms.Button btnDownloadAndDisplay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numCreditHours;
        private System.Windows.Forms.TextBox tbCourseDesc;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numMonth;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsStatusLabel;
        private System.Windows.Forms.Label lblClassAstric;
    }
}

